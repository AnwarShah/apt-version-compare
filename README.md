# Apt Version Comparison
This is a ruby script to compare which implemented the apt-version comparison algorithm.

It was originally translated from dpkg library into a python script here: https://bitbucket.org/excid3/winlibre.  

I just translated the python script into ruby

# How to use

Load or require the script. 


Usage:  

        vercmp(ver_string1, ver_string2)
        
Returns:  

        -1 if ver_string1 less than (<) ver_string2  
        0 if ver_string1 equal to (=) ver_string2  
        1 if ver_string1 greater than (>) ver_string2  